// ============================================================================ 
//  Copyright (c) 2019 - 2021, European Spallation Source ERIC
// 
//  The program is free software: you can redistribute it and/or modify it
//  under the terms of the BSD 3-Clause license.
//
//  This program is distributed in the hope that it will be useful, but WITHOUT
//  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
//  FITNESS FOR A PARTICULAR PURPOSE.
//  
//  Author  : Tomasz Brys
//  email   : tomasz.brys@ess.eu
//  Date    : 2021-03-23
//  version : 0.0.1 
// ============================================================================ 
//
// The function average takes as an input a waveform 
// and calculate min, max and average of the waveform
// The calculated minimum, maximum and average is sanding back to the db
//

#include <aSubRecord.h>
#include <registryFunction.h>
#include <epicsExport.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

static long average(aSubRecord *pRecord){


   double *tab = (double*)pRecord->a;   // pointer to waveform
   unsigned npoints = pRecord->nea;   // nr of points in the waveform
   double dcoffset  = *(double*)pRecord->b; //dc offset value for calculation
   double amplitude  = *(double*)pRecord->c; //maximum set amplitude value in mV
   //double result[40];  //array for the result
   double result;
   double deg;
   // initial values
   double min = tab[0];
   double max = tab[0];
   double average;
   double sum = 0;
   double x[40];
   for(unsigned int i = 0; i < npoints; i++){

      if(tab[i] > max)
         max = tab[i];

      if(tab[i] < min)
         min = tab[i];

      sum += tab[i];
      x[i] = (tab[i]/amplitude)-dcoffset;
      //printf("Inverse of cos(%.2f) = %.2lf in radians\n", x[i], result[i]);
   }

    average = sum / npoints;
    result = acos(x[1]);
    deg = result*(180/3.14);
    //printf("Value to be calculated=%f radian=%f deg=%f scaled value %f\n", x[1], result, deg, tab[1]);
   *(double*)pRecord->vala = min;
   *(double*)pRecord->valb = max;
   *(double*)pRecord->valc = average;
   *(double*)pRecord->vald = deg;

   //printf("[min/test/avr] = %f\t%f\t%f\n", min, test, average);
return 0;
}


epicsRegisterFunction(average);


//# - end of file
